# import os
# import pytest
# import shutil
# import sys
#
# from random import randint
#
from unittest.mock import patch

import src
from src.models import Book, Library
from src import config

books = [
    {
        'filename': 'OReilly.CSS.The.Missing.Manual.4th.Edition.1491918055.rar',
        'publisher': "O'Reilly Media",
        'title': "CSS The Missing Manual 4th Edition"
    },
    {
        'filename': 'Packt.Learning.Less.js.1782160663.rar',
        'publisher': "Packt Publishing",
        'title': "Learning Less js"
    },
    {
        'filename': 'Packt.Python.3.Object-Oriented.Programming.2nd.Edition.1784398780.rar',
        'publisher': "Packt Publishing",
        'title': "Python 3 Object-Oriented Programming 2nd Edition"
    },
    {
        'filename': 'Packt.Troubleshooting.PostgreSQL.1783555319.rar',
        'publisher': "Packt Publishing",
        'title': "Troubleshooting PostgreSQL"
    },
    {
        'filename': 'Apress.Learn.to.Program.with.Python.148421868X.rar',
        'publisher': "Apress",
        'title': "Learn to Program with Python"
    },
    {
        'filename': 'Apress.Material.Design.implementation.with.AngularJS.UI.Component.Framework.1484221893.rar',
        'publisher': "Apress",
        'title': "Material Design implementation with AngularJS UI Component Framework"
    },
    {
        'filename': 'Course.Technology.Concepts.of.Database.Management.8th.Edition.1285427106.rar',
        'publisher': "Cengage Learning",
        'title': "Concepts of Database Management 8th Edition"
    },
    {
        'filename': 'CreateSpace.Selenium.WebDriver.for.Functional.Automation.Testing.Part.1.1535420545.rar',
        'publisher': "CreateSpace",
        'title': "Selenium WebDriver for Functional Automation Testing Part 1"
    },
    {
        'filename': 'Oxford.University.Press.Algorithms.Design.and.Analysis.0199456666.rar',
        'publisher': "Oxford University Press",
        'title': "Algorithms Design and Analysis"
    },
    {
        'filename': 'Wiley.Adventures.in.Coding.1119232686.rar',
        'publisher': "Wiley",
        'title': "Adventures in Coding"
    },
    {
        'filename': 'Wiley.Computational.Physics.Problem.Solving.with.Python.3rd.Edition.3527413154.rar',
        'publisher': "Wiley",
        'title': "Computational Physics Problem Solving with Python 3rd Edition"
    },
    {
        'filename': 'Prentice.Hall.A.Practical.Guide.to.Linux.Commands.Editors'
                    '.and.Shell.Programming.3rd.Edition.013308504X.rar',
        'publisher': "Prentice Hall",
        'title': "A Practical Guide to Linux Commands Editors and Shell Programming 3rd Edition"
    },
]

extensions = config.FILE_EXTENSION


@patch('builtins.input', lambda x: 'n')
def test_get_publisher():
    for book in books:
        _book = Book(book['filename'])
        assert _book.get_publisher()[1] == book['publisher']


@patch('builtins.input', lambda x: 'n')
def test_get_title():
    for book in books:
        _book = Book(book['filename'])
        assert _book.get_title() == book['title']


@patch('builtins.input', lambda x: 'n')
def test_book_str():
    for book in books:
        _book = Book(book['filename'])
        assert str(_book) == "[{publisher}] {title}".format(publisher=book['publisher'], title=book['title'])

# def get_test_book_dir():
#     return os.path.join(os.getcwd(), 'test', 'books')
#
#
# def test_init():
#     os.mkdir(get_test_book_dir())
#
#     for book in books:
#         for i in range(randint(1, len(extensions))):
#             os.mknod(os.path.join(get_test_book_dir(), str(book) + extensions[i]))
#             os.system(
#                 'rar a -df -ep ' +  # df = delete files after compression -ep = exclude path
#                 os.path.join(get_test_book_dir(), str(book) + '.rar') +
#                 ' ' +
#                 os.path.join(get_test_book_dir(), str(book) + extensions[i])
#             )
#
#     assert len(os.listdir(get_test_book_dir())) == len(books)
#
#
# def test_init_library_class():
#     my_library = Library(get_test_book_dir())
#
#     assert my_library.root_directory == get_test_book_dir()
#     assert my_library.number_of_books == len(books)
#
# # def test_init_book_class():
# #     my_book = Book(
# #         os.path.join(get_test_book_dir(), books[1] + '.rar'),
# #         get_test_book_dir()
# #     )
# #     assert my_book.press_name == "O'Reilly"
# #     assert my_book.book_name == "CSS The Missing Manual 4th Edition"
# #     assert my_book.book_directory == "[O'Reilly] CSS The Missing Manual 4th Edition"
#
#
# # def test_get_press_name():
# #     book = Book(
# #         os.path.join(os.getcwd(), books[0]),
# #         os.getcwd()
# #     )
# #     assert str(book) == "[O'Reilly] CSS The Missing Manual 4th Edition"
# #     assert book.press_name == "O'Reilly"
# #     assert book.press_key == "OReilly"
# #     assert book.book_name == "CSS The Missing Manual 4th Edition"
#
# def test_destroy():
#     shutil.rmtree(get_test_book_dir())
#     assert os.path.exists(get_test_book_dir()) is False
