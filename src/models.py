import json
import os
import patoolib
import shutil

import isbnlib

from colorama import Fore, Style

from .config import *
from .exception import PublisherNotFoundError


class Book:
    def __init__(self, filename):
        self.filename = filename
        self.root_directory = BOOKS_DIRECTORY

        print("\n" + "Processing " + Fore.RED + self.filename + Fore.WHITE + "..." + Style.RESET_ALL + "\n")

        try:
            self.publisher_key, self.publisher = self._get_publisher()
            self.title = self._get_title()
        except PublisherNotFoundError:
            print("[" + Fore.RED + "ERROR" + Style.RESET_ALL + "] " +
                  "No publisher was found with this file: " +
                  Fore.GREEN + "{file}".format(file=self.filename) + Style.RESET_ALL)
            print()
            self.publisher = input("Please insert publisher: ")
            self.title = input("Please insert title: ")
            print()

        self._check_book()

        self.book_directory = str(self)
        self._print_separator()

    def __str__(self):
        return "[{publisher}] - {title}".format(
            publisher=self.publisher,
            title=self.title
        )

    def _get_publisher(self):
        """
        Analyze file name and try to obtain publisher from Library dict.
        Raise PublisherNotFoundError otherwise.

        :return: publisher or raise PublisherNotFoundError
        """
        for key in Library.publishers.keys():
            if self.filename.lower().startswith(key):
                return key, Library.publishers[key]
        else:
            raise PublisherNotFoundError

    def _get_title(self):
        """
        Remove publisher, dots, isbn and extension from the file's name.

        :return: str
        """
        _filename = self.filename[len(self.publisher_key)+1:]
        _filename = " ".join(_filename.split(".")[:-2])

        return _filename

    def _check_book(self):
        print("Book found: " + Fore.GREEN + str(self) + Style.RESET_ALL)

        user_answer = input("Do you want to change it? Y/[N] ")
        if user_answer.lower().startswith("y"):
            print()
            self.publisher = input("Please insert publisher: ")
            self.title = input("Please insert title: ")
            print()
            self._check_book()

    def _get_isbn(self):
        return self.filename.split(".")[-2:-1][0]

    def _get_files_presence(self):
        files = dict()

        files['pdf'] = False
        files['mobi'] = False
        files['azw3'] = False
        files['epub'] = False
        files['source_code'] = False

        for file in os.listdir(self.root_directory + "/" + self.book_directory):
            if "pdf" in file:
                files['pdf'] = True
            elif "mobi" in file:
                files['mobi'] = True
            elif "azw3" in file:
                files['azw3'] = True
            elif "epub" in file:
                files['epub'] = True
            elif "rar" in file:
                files['source_code'] = True

        return files

    @staticmethod
    def _print_separator():
        print()
        print(Fore.BLUE + "#" * 125)
        print("#" * 125 + Style.RESET_ALL)

    def create_directory(self):
        """
        Create directory if it doesn't exist.

        :return: none
        """
        if not os.path.exists(os.path.join(self.root_directory, self.book_directory)):
            os.makedirs(os.path.join(self.root_directory, self.book_directory))

    def extract(self):
        """
        Extract file in the created directory.

        :return: none
        """
        self.create_directory()

        patoolib.extract_archive(
            os.path.join(self.root_directory, self.filename),
            outdir=os.path.join(self.root_directory, self.book_directory),
            verbosity=-1
        )

    def delete_unnecessary_files(self):
        """
        Remove all the unnecessary files in the book directory.

        :return: none
        """

        for filename in os.listdir(os.path.join(self.root_directory, self.book_directory)):
            if os.path.splitext(filename)[1] not in FILE_EXTENSION:
                if os.path.splitext(filename)[0].lower() == "foxebook.net":
                    os.remove(os.path.join(self.root_directory, self.book_directory, filename))
                else:
                    os.system("clear")
                    print("File: " + Style.DIM + Fore.GREEN + filename + Style.RESET_ALL)
                    print("Directory: " + Style.DIM + Fore.YELLOW + self.book_directory + Style.RESET_ALL)
                    answer = input("Delete? [Y]/N ")
                    if answer.lower().startswith("y") or not answer:
                        os.remove(os.path.join(self.root_directory, self.book_directory, filename))

    def rename_files_in_directory(self):
        """
        Rename all the ebook files in book directory.

        :return: none
        """
        for file in os.listdir(os.path.join(self.root_directory, self.book_directory)):
            os.rename(
                os.path.join(self.root_directory, self.book_directory, file),
                os.path.join(self.root_directory, self.book_directory, str(self) + os.path.splitext(file)[1])
            )

    def create_meta_file(self):
        isbn = self._get_isbn()
        print("\n" + "Processing " + Fore.RED + self.filename + Fore.WHITE + "..." + Style.RESET_ALL + "\n")

        meta = dict()

        meta['authors'] = ["", ""]
        meta['edition'] = ""
        meta['files'] = self._get_files_presence()
        meta['isbn'] = ""
        meta['language'] = ""
        meta['publisher'] = ""
        meta['subtitle'] = ""
        meta['title'] = ""
        meta['year'] = ""

        try:
            _meta = isbnlib.meta(isbn)
            meta['authors'] = _meta['Authors']
            meta['isbn'] = _meta['ISBN-13']
            meta['language'] = _meta['Language']
            meta['publisher'] = _meta['Publisher']
            meta['title'] = _meta['Title']
            meta['year'] = _meta['Year']

        except:
            print("Book meta not found!")

        print(json.dumps(meta, indent=4, sort_keys=True))

        with open(self.root_directory + "/" + self.book_directory + "/META", "w") as meta_file:
            json.dump(meta, meta_file, indent=4, sort_keys=True)

        self._print_separator()


class Library:

    publishers = publishers

    def __init__(self, book_directory):
        self.root_directory = book_directory
        self.number_of_books = 0
        self.books = []

        for filename in os.listdir(self.root_directory):
            if os.path.isfile(os.path.join(self.root_directory, filename)) and filename.endswith(".rar"):
                self.number_of_books += 1
                self.books.append(Book(filename))
            elif filename != DELETE_ME_FOLDER:
                os.system("clear")
                print("[" + Fore.RED + "ERROR" + Style.RESET_ALL + "] " +
                      "Found: " + Style.DIM + Fore.GREEN + "{file_found}".format(file_found=filename) + Style.RESET_ALL)
                answer = input("Delete? [Y]/N ")
                if answer.lower().startswith("y") or not answer:
                    os.remove(os.path.join(self.root_directory, filename))

    def run(self):
        self.extract_files()
        self.move_rar_to_delete_folder()
        self.delete_unnecessary_files()
        self.rename_files()
        self.create_meta_files()

    def extract_files(self):
        """
        Extract all the .rar files in the root directory.

        :return: none
        """

        for book in self.books:
            book.extract()

    def move_rar_to_delete_folder(self):
        """
        Create DELETE_ME_FOLDER directory if it doesn't exists and move all rar files to this directory.

        :return: none
        """

        if not os.path.exists(os.path.join(self.root_directory, DELETE_ME_FOLDER)):
            os.makedirs(os.path.join(self.root_directory, DELETE_ME_FOLDER))

        for filename in os.listdir(self.root_directory):
            if filename.endswith(".rar"):
                shutil.move(
                    os.path.join(self.root_directory, filename),
                    os.path.join(self.root_directory, DELETE_ME_FOLDER)
                )

    def delete_unnecessary_files(self):
        """
        Remove all files in every book's folder that aren't book, like txt files.

        :return: none
        """

        for book in self.books:
            book.delete_unnecessary_files()

    def rename_files(self):
        """
        Rename all the book's files.

        :return: none
        """

        for book in self.books:
            book.rename_files_in_directory()

    def create_meta_files(self):
        """
        Create all the META files.

        :return: none
        """

        for book in self.books:
            book.create_meta_file()
