# colorama colors
# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL

# List of all publishers

publishers = {
    'a.book.apart': "A Book Apart",
    'apigee': "Apigee",
    'apress': "Apress",
    'aw': "Addison-Wesley",
    'bleeding.edge.press': "Bleeding Edge Press",
    'cengage': 'Cengage Learning',
    'course.technology': "Cengage Learning",
    'crc': "CRC Press",
    'createspace': "CreateSpace",
    'elsevier': "Elsevier",
    'ios.press': "IOS Press",
    'leanpub': "Leanpub",
    'manning': "Manning Publications",
    'mgh': "McGraw-Hill",
    'new.riders': "New Riders",
    'no.starch.press': "No Starch Press",
    'nsp': "No Starch Press",
    'oreilly': "O'Reilly Media",
    'oxford.university.press': "Oxford University Press",
    'packt': 'Packt Publishing',
    'pearson': 'Pearson Education',
    'pe.press': 'PE Press',
    'pragmatic': 'The Pragmatic Bookshelf',
    'prentice.hall': "Prentice Hall",
    'quantatrisk.ebooks': 'QuantAtRisk eBooks',
    'sams.publishing': 'Sams Publishing',
    'sams': 'Sams Publishing',
    'sitepoint': "SitePoint",
    'springer': "Springer",
    'sybex': "Sybex",
    'waveland.press': "Waveland Press",
    'wiley': "Wiley",
    'world.scientific': "World Scientific",
    'wrox': "Wrox",
    # '': "Two Scoops Press",
    # '': "Syncfusion",
    # '': "Smashing",
    # '': "The Pragmatic Programmers",
    # '': "InfoQ",
    # '': "BCS",
    # '': "Peachpit Press",
    # '': "Mercury",
    # '': "Addison-Wesley",
    # '': "Cambridge University Press",
    # '': "Green Tea Press",
    # add other press names
}

FILE_EXTENSION = ('.pdf', '.epub', '.mobi', '.azw3', '.azw4', '.zip')

# This directory contains all the rar files that will be deleted
# Why? Because if the program somehow fails, I have a full backup :-)
DELETE_ME_FOLDER = 'delete_me_folder'

# MEGASync's books directory
# MEGA_BOOKS_DIRECTORY = 'nice'  # shared folder? '~/Books'
BOOKS_DIRECTORY = '/home/yverek/Documents/Libri'
