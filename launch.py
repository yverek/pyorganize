import os
import sys

from src.models import Library
from src.config import BOOKS_DIRECTORY

if __name__ == "__main__":
    sys.stdout.write("\x1b[8;{rows};{cols}t".format(rows=35, cols=150))
    os.system("clear")
    my_library = Library(BOOKS_DIRECTORY)
    my_library.run()
